package by.sc.kafkaconsumer.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MsgDto {
    private int id;
    private String text;
}
