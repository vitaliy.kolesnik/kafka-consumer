package by.sc.kafkaconsumer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RateDto {

    @JsonProperty("USD_in")
    private String  usdIn;

    @JsonProperty("USD_out")
    private String  usdOut;

    @JsonProperty("EUR_in")
    private String  eurIn;

    @JsonProperty("EUR_out")
    private String  eurOut;

    @JsonProperty("RUB_in")
    private String  rubIn;

    @JsonProperty("RUB_out")
    private String  rubOut;
}
