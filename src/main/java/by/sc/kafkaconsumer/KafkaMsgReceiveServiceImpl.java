package by.sc.kafkaconsumer;

import by.sc.kafkaconsumer.bot.NotifyBot;
import by.sc.kafkaconsumer.dto.MsgDto;
import by.sc.kafkaconsumer.dto.RateDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 * @author Timofei Lyahor (t.lyahor@softclub.by)
 */
@Service
@Slf4j
public class KafkaMsgReceiveServiceImpl {

    private final NotifyBot bot;

    private final Long CHAT_ID = 241190540L;

    public KafkaMsgReceiveServiceImpl(NotifyBot bot) {
        this.bot = bot;
    }

//    @KafkaListener(id = "1234", topics = {"softclub"}, containerFactory = "singleFactory")
//    public void consume(MsgDto dto) {
//        log.info(dto.getText());
//
//        SendMessage message = new SendMessage();
//
//        try {
//            message.setText(dto.getText());
//            message.setChatId(CHAT_ID);
//            bot.execute(message);
//        } catch (TelegramApiException e) {
//            log.error(e.getMessage(), e);
//        }
//
//    }

    @KafkaListener(id = "1234", topics = {"softclub"}, containerFactory = "rateFactory")
    public void consume(RateDto dto) {

        log.info(dto.toString());

        SendMessage message = new SendMessage();

        try {
            message.setText(dto.toString());
            message.setChatId(CHAT_ID);
            bot.execute(message);
        } catch (TelegramApiException e) {
            log.error(e.getMessage(), e);
        }
    }

}
