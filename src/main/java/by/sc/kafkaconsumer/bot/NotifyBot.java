package by.sc.kafkaconsumer.bot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class NotifyBot extends TelegramLongPollingBot {

    @Value(value = "${spring.bot.name}")
    private String botName;

    @Value(value = "${spring.bot.token}")
    private String botToken;

    private TelegramBotsApi telegramBotsApi;

    static {
        ApiContextInitializer.init();
    }

    @PostConstruct
    public void init() {
        telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(this);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }


    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();

        if (message != null && message.getText() != null) {
            SendMessage msg = new SendMessage();
            log.info(String.valueOf(message.getText()));
            msg.setChatId(message.getChatId());
            msg.setText(message.getText());

            try {
                this.execute(msg);
            } catch (TelegramApiException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public String getBotUsername() {
        return botName;
    }

    public String getBotToken() {
        return botToken;
    }
}